package com.example.demo;

import org.junit.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.lang.Math;

@SpringBootApplication
public class CalculadoraApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculadoraApplication.class, args);
	}

	public static double divide(double i, double y) {
		return i/y;
	}
	
	public static double raizCubica(double y) {		
		return Math.cbrt(y);
	}

	public static double potencia(double x, double y) {
		return Math.pow(x, y);
	}

	public static double multiplica(double x, double y) {
		return x*y;
	}
	
}
