package com.example.demo;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class calculadoraAPI {
	
	@RequestMapping(value = "/raizCubica", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Double raizCubica ( @RequestParam double value ){
		return CalculadoraApplication.raizCubica(value);
	}

	@RequestMapping(value = "/divisao", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Double divisao ( @RequestParam double dividendo, @RequestParam double divisor ){
		return CalculadoraApplication.divide(dividendo, divisor);
	}	

	@RequestMapping(value = "/potencia", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Double potencia ( @RequestParam double base, @RequestParam double expoente ){
		return CalculadoraApplication.potencia(base, expoente);
	}	

	@RequestMapping(value = "/multiplica", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Double multiplica ( @RequestParam double fator1, @RequestParam double fator2 ){
		return CalculadoraApplication.multiplica(fator1, fator2);
	}		
	
	@RequestMapping(value = "/test", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String testAPI ( ){
		return "API funcionando!";
	}
}
